# Precompiled binaries
This folder contains some precompiled binaries used for the initial tests.  

## QUIC-GO
`client_go` and `server_go` are a QUIC client and server from the original repository of 
[QUIC-Go](https://github.com/lucas-clemente/quic-go).

You can find the build instructions in the landing page.

## MP-QUIC

`client_mp` and `server_mp` are from the original [MP-QUIC](https://github.com/qdeconinck/mp-quic).
Installations instructions are in the [artifacts](https://multipath-quic.org/2017/12/09/artifacts-available.html)
 webpage.
 
 ## proto-quic
 
 From official [proto-quic](https://github.com/google/proto-quic) repository, which includes
 the building instructions.
 
 This repo seems to be deprecated so eventually it might disappear. 
 
 